const employeeData = require(`./1-arrays-jobs.cjs`);

const salaries = employeeData.map(obj => {

    let salary = obj.salary;

    obj.salary = Number(salary.substring(1));

    return obj;
});

console.log(salaries);