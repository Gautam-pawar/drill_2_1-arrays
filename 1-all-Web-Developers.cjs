const employeeData = require(`./1-arrays-jobs.cjs`);

const webDeveloper = employeeData.reduce((accumulator, obj) => {

    if (obj.job.includes('Web Developer')) {

        accumulator.push(obj.first_name + " " + obj.last_name);
    }

    return accumulator;
}, []);

console.log(webDeveloper);