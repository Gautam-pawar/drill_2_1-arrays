
const employeeData = require(`./1-arrays-jobs.cjs`);

function factorer(obj) {

   let temp = obj["salary"];

   let convertedSalary = Number(temp.substring(1)) * 10000;

   obj["converted-salary"] = convertedSalary;

   return obj;

}

console.log(employeeData.map(factorer));  