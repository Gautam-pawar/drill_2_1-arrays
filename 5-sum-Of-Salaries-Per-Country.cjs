
const employeeData = require(`./1-arrays-jobs.cjs`);

const resultantObj = employeeData.reduce((accumulator, obj) => {

    let Salary = Number(obj["salary"].substring(1));

    let Country = obj["location"];

    if (accumulator[Country]) {

        accumulator[Country] += Salary;

    } else {

        accumulator[Country] = Salary;
    }

    return accumulator;

}, {});

console.log(resultantObj);