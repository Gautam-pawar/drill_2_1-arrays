
const employeeData = require(`./1-arrays-jobs.cjs`);

const resultantObj = employeeData.reduce( (accumulator , obj) => {

    let Salary = Number(obj.salary.substring(1));

    let Country = obj.location;

    if (accumulator[Country]) {

        accumulator[Country][0] = Salary;

        accumulator[Country][1] += 1 ;

    } else {

        let avgData = [];

        avgData[0] = Salary;

        avgData[1] = 1;

        accumulator[Country] = avgData;
    }

    return accumulator ;

},{});


for (let key in resultantObj) {
    resultantObj[key] = (resultantObj[key][0] / resultantObj[key][1]).toFixed(2);
}

  console.log(resultantObj);