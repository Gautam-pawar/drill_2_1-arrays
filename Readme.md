# Drill on Arrays in Javascript

## Drill contains various operations on data receveied in the form of array.

### Following are the operations performed in the drill

1. Filtering Web developer employees from given data.

2. convrting Salaries saved in string to Number format.

3. Converting the the factored salaries into actual amount.

4. Calculating sum of salaries in the data.

5. Calculating sum of Salaries per country.

6. Calculating average salaries per country.

